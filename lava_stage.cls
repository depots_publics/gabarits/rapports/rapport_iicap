% LAVA
% Alain Batailly - ao�t 2015
%
% fichier classe inspir� de la classe ecn10.cls par Mathias Legrand

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
%\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions

\LoadClass[letterpaper,oneside]{book}

\RequirePackage[T1]{fontenc}
\RequirePackage[latin1]{inputenc}
\RequirePackage{graphicx}
\RequirePackage{xcolor}
\RequirePackage{fancyhdr}
\RequirePackage{fancybox}
\RequirePackage{lscape}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{amsfonts}
\RequirePackage{amscd}
\RequirePackage[margin=20pt,font=small,labelfont={color=bleufonce,bf},labelsep=endash]{caption}
\RequirePackage{psfrag}
\RequirePackage{nameref}
\RequirePackage{multirow}
\RequirePackage[colorlinks,linktocpage=true,bookmarks=true,pagebackref,breaklinks=true,citecolor=bleufonce,bookmarksnumbered=true,linkcolor=bleufonce,urlcolor=bleufonce]{hyperref}
\RequirePackage{array}
\RequirePackage[square,numbers]{natbib}
\RequirePackage{lmodern}
\RequirePackage{rotating}
\RequirePackage[english]{babel}
\RequirePackage{upgreek}

% definition de couleurs pour les liens Hyperref dans le texte
\definecolor{plo}{RGB}{225,103,0}
\definecolor{grayy}{RGB}{225,225,225}
\definecolor{grayF}{RGB}{225,225,225}
\definecolor{plg}{rgb}{0.1,0.7,0.1}
\definecolor{colorsection}{RGB}{81,81,66}
\definecolor{colorsubsection}{RGB}{101,101,86}
\definecolor{colorsubsubsection}{RGB}{121,121,106}
\definecolor{colorparagraph}{RGB}{0,0,0}
\definecolor{colorChapNum}{RGB}{91,91,76}

% Couleur des diff�rents liens et num�ros
\definecolor{ocre}{RGB}{145,5,5}
\definecolor{bleufonce}{RGB}{5,5,145}
\definecolor{grisclair}{RGB}{220,220,220}

% caption en fran�ais
\addto\captionsenglish{\def\contentsname{\raggedright\Huge\bfseries\sffamily Table des mati�res}}

% d�finition du d romain de d�rivation
\newcommand{\ud}{\mathrm{d}}

% param�tres de la table des mati�res
\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{2}

% d�finition des captions des objets flottants
\newcommand{\chapternumfont}{\normalfont\fontsize{50}{30}\bfseries\selectfont}
\newcommand{\annexnumfont}{\normalfont\fontsize{50}{15}\bfseries\selectfont}

% d�finition de l'ent�te des chapitres non num�rot�s
\newcommand{\StarChapter}[1]{
\chapter*{\vspace*{0\p@}\raggedleft\bfseries\sffamily #1 \vspace*{50\p@}}
\addcontentsline{toc}{chapter}{#1}
\markboth{\normalfont\sffamily #1}{}
\par}

% d�finition de l'ent�te de la table des mati�res
\renewcommand{\tableofcontents}{\pagestyle{empty}
\chapter*{\raggedleft\bfseries\Huge\sffamily\contentsname}
\@starttoc{toc}}

% environnement de liste
\newenvironment{maliste}%
{\begin{list}%
    {$-$}%
    {%\setlength{\labelwidth}{30pt}%
     \setlength{\leftmargin}{30pt}%
     \setlength{\itemsep}{30pt}}}%
{\end{list}}


% d�finition de l'ent�te des chapitres
\def\@makechapterhead#1{%
\vspace*{-20\p@}
  { \raggedleft \normalfont\sffamily
    \ifnum \c@secnumdepth >\m@ne
      \if@mainmatter
          \raggedright\bfseries\chapternumfont{\textcolor{bleufonce}{\thechapter}}\par\nobreak
          \vspace*{10pt}
        \fi
      \fi
    \begin{flushleft}\begin{minipage}[t]{14cm}\raggedright\Huge\bfseries\sffamily #1\end{minipage}\end{flushleft}
    \vspace*{20\p@}%
}}

% d�finition de la bibliographie
\renewenvironment{thebibliography}[1]{
\setlength{\parindent}{0cm}
\chapter*{\vspace*{-60\p@}\raggedright\bfseries\Huge\sffamily R\'ef\'erences bibliographiques \vspace*{50\p@}}
\markboth{\normalfont\normalsize\sffamily R\'ef\'erences bibliographiques}{}
\addcontentsline{toc}{chapter}{R\'ef\'erences bibliographiques}
\par
\list{\@biblabel{\@arabic\c@enumiv}}
{\settowidth\labelwidth{\@biblabel{#1}}}
\leftmargin\labelwidth
\advance\leftmargin\labelsep
\@openbib@code
\usecounter{enumiv}
\let\p@enumiv\@empty
\renewcommand\theenumiv{\@arabic\c@enumiv}
\sloppy
\clubpenalty4000
\@clubpenalty \clubpenalty
\widowpenalty4000%
\sfcode`\.\@m
{\def\@noitemerr
{\@latex@warning{Empty `thebibliography' environment}}%
\endlist}
}



% d�finition de l'apparence des sections et sous-sections
\renewcommand{\@seccntformat}[1]{\llap{\textcolor{bleufonce}{\csname the#1\endcsname}\hspace{0.5em}}}
                                      
\renewcommand{\section}{\@startsection{section}{1}{\z@}
                       {-4ex \@plus -1ex \@minus -.4ex}%
                       {1ex \@plus.2ex }%
                       {\normalfont\Large\sffamily\bfseries}}
\renewcommand{\subsection}{\@startsection {subsection}{2}{\z@}
                          {-3ex \@plus -0.1ex \@minus -.4ex}%
                          {0.5ex \@plus.2ex }%
                          {\normalfont\fontsize{12}{16}\sffamily\bfseries}}
\renewcommand{\subsubsection}{\@startsection {subsubsection}{3}{\z@}
                          {-2ex \@plus -0.1ex \@minus -.2ex}%
                          {0.2ex \@plus.2ex }%
                          {\normalfont\fontsize{11}{16}\sffamily\bfseries}}
                          
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
                                    {-2ex \@plus-.2ex \@minus .2ex}%
                                    {0.1ex}%
                                    {\normalfont\fontsize{11}{13}\sffamily\bfseries}}
\renewcommand\subparagraph{\@startsection{subparagraph}{5}{\z@}%
                                       {-2ex \@plus-.1ex \@minus .2ex}%
                                       {0.1ex}%
                                      {\normalfont\normalsize\sffamily\bfseries}}

